<?php

namespace PeterNikonov\LaravelTablePrefix;

class TablePrefix
{
    protected $prefix = null;

    protected $delimeter = '_';

    /**
     * @param $prefix Prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * @return mixed Prefix
     */
    public function getPrefix()
    {
        if (null == $this->prefix) {
            return;
        }

        return $this->prefix;
    }

    /**
     * @return string
     */
    public function getDelimeter(): string
    {
        return $this->delimeter;
    }

    /**
     * @param string $delimeter
     */
    public function setDelimeter(string $delimeter)
    {
        $this->delimeter = $delimeter;
    }
}
