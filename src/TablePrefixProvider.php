<?php

namespace PeterNikonov\LaravelTablePrefix;

use Illuminate\Support\ServiceProvider;

class TablePrefixProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Include the package classmap autoloader
        if (\File::exists(__DIR__ . '/../vendor/autoload.php')) {
            include __DIR__ . '/../vendor/autoload.php';
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('TablePrefix', function () {
            return $this->app->make('PeterNikonov\LaravelTablePrefix\TablePrefix');
        });
    }
}
