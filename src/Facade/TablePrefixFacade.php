<?php

namespace PeterNikonov\LaravelTablePrefix\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * Class TablePrefix
 *
 * @package peter-nikonov/laravel-table-prefix
 *
 * @method static setPrefix
 * @method static getPrefix
 * @method static setDelimeter
 * @method static getDelimeter
 */
class TablePrefix extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'TablePrefix';
    }

}