<?php

namespace PeterNikonov\LaravelTablePrefix;

use PeterNikonov\LaravelTablePrefix\Facade\TablePrefix;

trait TablePrefixTrait
{
    public function setPrefix()
    {
        if (null === TablePrefix::getPrefix()) {
            return;
        }

        $this->table = TablePrefix::getPrefix() . TablePrefix::getDelimeter() . $this->getTable();
    }
}
