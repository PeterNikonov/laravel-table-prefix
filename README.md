# Laravel Table Prefix

Allows you to use a table prefix with standard Laravel models.

### Installation

Install via composer:

```
composer require peter-nikonov/laravel-table-prefix
```

## Usage

Somewhere in your application: 

```php
use PeterNikonov\LaravelTablePrefix\Facade\TablePrefix; 

TablePrefix::setPrefix('instanceId');

```

In model class:

```php
use PeterNikonov\LaravelTablePrefix\TablePrefixTrait;
use Illuminate\Database\Eloquent\Model;

class MyModel extends Model 
{
    use TablePrefixTrait;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->setPrefix();
    }
}
```

## Thanks

https://github.com/yassine-khachlek/laravel-package-example